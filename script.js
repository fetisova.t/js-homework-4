/**
 * Created on 23.06.2019.
 */
//Опишите своими словами, что такое метод обьекта
//Метод объекта - это по сути функция, встроенная в объект, которая позволяет производить определенный заданный набор действий с этим объектом.

function createNewUser() {
    let newUser = {
        firstName : prompt('Enter your first name:'),
        lastName : prompt('Enter your last name:'),
        getLogin (){
            return this.firstName[0].toLowerCase()+this.lastName.toLowerCase();
        }
    };
    return newUser;
}
console.log(createNewUser().getLogin());

//Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser2() {
    let newUser = {
        getLogin2 (){
            return this.firstName[0].toLowerCase()+this.lastName.toLowerCase();
        }
    };
    Object.defineProperty(newUser, "firstName", {
        value: prompt('Enter your first name:'),
        writable: false, // запретить присвоение
        configurable: false // запретить удаление
    });
    Object.defineProperty(newUser, "lastName", {
        value: prompt('Enter your last name:'),
        writable: false, // запретить присвоение
        configurable: false // запретить удаление
    });
    Object.defineProperty(newUser, "getLogin2", {
        enumerable: false
    });
    return newUser;
}
//это все на что меня хватило :( про сеттер, так и не поняла
